var avPlayObject = window['webapis']['avplay'];
var button = document.getElementById('btn');
var progress = document.getElementById('progress');
var asyncPrepared = false;

var q480p = document.getElementById('480p');
var q720p = document.getElementById('720p');
var q1080p = document.getElementById('1080p');

var state = 'sync';
var videoUrl = {
	'480p': 'http://192.168.1.151/tizenPrepareTest/samples/elyzium.mp4',
	'720p': 'http://192.168.1.151/tizenPrepareTest/samples/p720.webm',
	'1080p': 'http://192.168.1.151/tizenPrepareTest/samples/p1080.webm'
};
var currentQuality = q480p;

var setQuality = function(quality) {
	currentQuality.style.background = 'grey';
	quality.style.background = 'blue';
	currentQuality = quality;
};

setQuality(currentQuality);

var setButtonCaption = function(state) {
	button.innerHTML = state;
};
setButtonCaption(state);

var startTime = 0;
var printTime = function() {
	document.getElementById('time').innerHTML = (Date.now() - startTime).toFixed(2);
};
var onBtnClick = function() {
	button.style.backgroundColor = 'grey';
	startTime = Date.now();
	setTimeout(function() {
		console.log('click', state);
		if (state === 'sync') {
			try {
				prepare();
				button.style.backgroundColor = 'green';
				printTime();
				state = 'async';
				setButtonCaption(state);
			} catch (e) {
				console.log(e);
			}
		} else if (state === 'async') {
			prepareAsync();
			//asyncPrepared = true;
			stop();
			button.style.backgroundColor = 'green';
			printTime();
			state = 'sync';
			setButtonCaption(state);
		}
	}, 0);
};

var prepare = function() {
	avPlayObject.close();
	var url = videoUrl[currentQuality.innerHTML];
	//url = url + '?_=' + Math.random();
	avPlayObject.open(url);
	avPlayObject.prepare();
	avPlayObject.play();
	avPlayObject.setDisplayRect(0, 0, 1280, 720);
};

var stop = function() {
	//if (asyncPrepared) {
	//	asyncPrepared = false;
	//}

	try {
		webapis.avplay.stop();
		console.log("Current state: " + webapis.avplay.getState());
	} catch (e) {
		console.log("Current state: " + webapis.avplay.getState());
		console.log(e);
	}
};

var prepareAsync = function() {
	avPlayObject.close();
	avPlayObject.open(videoUrl[currentQuality.innerHTML]);
	avPlayObject.prepareAsync(function() {
		console.log('prepareAsync finished');
		//if (asyncPrepared) {
			avPlayObject.play();
			avPlayObject.setDisplayRect(0, 0, 1280, 720);
		//}
	}, function(err) {
		console.log('error on prepareAsync: ', err);
	});
};

var setWidth = function(element, width) {
	element.style.width = width + 'px';
};

setWidth(progress, 0);
button.onclick = onBtnClick;

q480p.onclick = function() {
	setQuality(q480p);
};
q720p.onclick = function() {
	setQuality(q720p);
};
q1080p.onclick = function() {
	setQuality(q1080p);
};

setInterval(function() {
	if (parseInt(progress.style.width, 10) >= 1264) {
		setWidth(progress, 0);
	}
	setWidth(progress, parseInt(progress.style.width, 10) + 10);
}, 500);
